<?php

/**
 * @file
 * Default theme implementation to display a vocabulary.
 *
 * Available variables:
 * - $name: (deprecated) The unsanitized name of the vocabulary. Use $vocabulary_name
 *   instead.
 * - $content: An array of items for the content of the vocabulary (fields and
 *   description). Use render($content) to print them all, or print a subset
 *   such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $vocabulary_url: Direct URL of the current vocabulary.
 * - $vocabulary_name: Name of the current vocabulary.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the following:
 *   - taxonomy-vocabulary: The current template type, i.e., "theming hook".
 *   - vocabulary-[vocabulary-name]: The vocabulary to which the vocabulary belongs to.
 *     For example, if the vocabulary is a "Tag" it would result in "vocabulary-tag".
 *
 * Other variables:
 * - $vocabulary: Full vocabulary object. Contains data that may not be safe.
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $page: Flag for the full page state.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the vocabulary. Increments each time it's output.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_taxonomy_vocabulary()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div id="taxonomy-vocabulary-<?php print $vocabulary->vid; ?>" class="<?php print $classes; ?>">

  <?php if (!$page): ?>
    <h2><a href="<?php print $vocabulary_url; ?>"><?php print $vocabulary_name; ?></a></h2>
  <?php endif; ?>

  <div class="content">
    <?php print render($content); ?>
  </div>

</div>
